1. Logger.log and the console differ because of logging filters found in the resources/logger.properties file
2. This come's from JUnit's org.junit.jupiter.engine.execution.ConditionEvaluator
3. It requires that the provided exception is thrown, if nothing is thrown or something is thrown, the test will fail.
4.1 The serialVersionUID is used to make sure that upon deserialization of a serializable object, the loaded class is compatible with the erialized object.
4.2. We need to override this so that TimerException's method can be set in it's parent's (Exception class) attribute. Without this the Exceptions message would be the default message or null.
4.3. We only need to override message and cause to allow for message overriding, and correctly show the chain of exceptions.
5. Debugger shows that this statc block is run a single time and loads the configuration file that powers the logger. (Operates similar to a constructor)
6. REAMDME.md is a file format the bit bucket (and gitHub) recognize to allow HTML markup to be generated based on the readme file. It allows for easy pushing of readme files to be translated into repo HTML pages.
7. This test was failing, as the failOverTest's Assertions.assert expected a Timer exception, but recieved a nullpointer excpetion. This was fixed by allocating the timeNow and not allowing it to be returned as null.
8. THe actual issue was timeNow being passed into the method(long time) function as null. The order of calling from debugging was Tester.failOverTest()->Timer.timeMe()->method()
11. NullPointerException is a run-time exception, and subclass Checked Exception.
 
